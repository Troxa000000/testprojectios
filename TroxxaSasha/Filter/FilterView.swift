//
//  FilterView.swift
//  TroxxaSasha
//
//  Created by Zhekon on 24.06.2020.
//  Copyright © 2020 Александр. All rights reserved.
//

import UIKit

class FilterView: UIView {
    
    let filtersTableView = UITableView()
    let applyButton = UIButton()
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit() {
        self.backgroundColor = .white
        
        addSubview(filtersTableView)
        
        addSubview(applyButton)
        applyButton.backgroundColor = .black
        applyButton.setTitle("APPLY", for: .normal)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        filtersTableView.pin.top().horizontally().bottom(100)
        applyButton.pin.bottom(25).horizontally(25).height(50)
    }
}
