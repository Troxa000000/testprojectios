//
//  FilterVIewController.swift
//  TroxxaSasha
//
//  Created by Zhekon on 24.06.2020.
//  Copyright © 2020 Александр. All rights reserved.
//

import UIKit

class FilterVIewController: UIViewController, UITableViewDelegate, UITableViewDataSource  {
    
    let mainView = FilterView()
    private let reuseIdentifierTableView = "CellTableView"
    var apiData = AllCategories()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        сonfigView()
        getApiData()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = true
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.apiData.drinks?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellFilter = tableView.dequeueReusableCell(withIdentifier: reuseIdentifierTableView, for: indexPath) as! FilterCell
        cellFilter.groupCellname.text = self.apiData.drinks![indexPath.row].strCategory
        cellFilter.selectionStyle = .none
        cellFilter.tapAction  = { (cell) in
            cellFilter.checkButton.isSelected = !cellFilter.checkButton.isSelected
            if  !cellFilter.checkButton.isSelected {
                cellFilter.checkButton.setImage(UIImage(named: "check"), for: .normal)
            }else {
                cellFilter.checkButton.setImage(UIImage(), for: .normal)
            }
        }
        return cellFilter
    }
    
    func сonfigView() {
        self.view.addSubview(mainView)
        self.mainView.frame = self.view.bounds
        self.navigationController?.navigationBar.isHidden = false
        self.title = "Filters"
        mainView.filtersTableView.delegate = self
        mainView.filtersTableView.dataSource = self
        mainView.filtersTableView.register(FilterCell.self, forCellReuseIdentifier: reuseIdentifierTableView)
        self.mainView.filtersTableView.separatorStyle = .none
        self.mainView.applyButton.addTarget(self, action: #selector(applyPressed), for: .touchUpInside)
        navigationController?.navigationBar.tintColor = .black
    }
    
    @objc func applyPressed() {
        self.navigationController?.popViewController(animated: true)
    }
    
    //Mark: Work withApi
    func getApiData() {
        getAllCategories() { (result) in
            switch result {
            case .success(let data):
                self.apiData = data
                self.mainView.filtersTableView.reloadData()
            case .partialSuccess( _): break
            case .failure(let error):
                print(error)
            }
        }
    }
}

