//
//  DataApi.swift
//  TroxxaSasha
//
//  Created by Александр on 23.06.2020.
//  Copyright © 2020 Александр. All rights reserved.
//

import UIKit
import Foundation


struct AllData : Codable {
    var drinks: [CockTailDetail]?
}

struct CockTailDetail : Codable {
    let strDrink: String?
    let strDrinkThumb: String?
    let idDrink: String?
}

struct AllCategories : Codable {
    var drinks: [Categories]?
}

struct Categories : Codable {
    let strCategory: String?
}
