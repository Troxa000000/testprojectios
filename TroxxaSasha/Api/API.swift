//
//  File.swift
//  TroxxaSasha
//
//  Created by Александр on 23.06.2020.
//  Copyright © 2020 Александр. All rights reserved.
//

import Foundation

enum NewResult<Value>{
    case success(Value)
    case partialSuccess(String)
    case failure(Error)
}

//MARK: Get all cocktails
func getAllCocktails(completion: ((NewResult<AllData>) -> Void)?){
    var urlComponents = URLComponents()
    urlComponents.scheme = "https"
    urlComponents.host = "www.thecocktaildb.com"
    urlComponents.path = "/api/json/v1/1/filter.php"
    urlComponents.queryItems = [URLQueryItem(name: "c", value: "Ordinary Drink")]
    guard let url = urlComponents.url else {
        print("__error: Could not create URL from components")
        return
    }
    var request = URLRequest(url: url)
    request.setValue("application/json", forHTTPHeaderField: "Content-Type")
    request.setValue("application/json", forHTTPHeaderField: "Accept")
    request.httpMethod = "GET"

    let session = URLSession.shared
    let task = session.dataTask(with: request) { (responseData, response, error) in
        DispatchQueue.main.async {
            if let error = error {
                completion?(.failure(error))
            } else if let jsonData = responseData {
                let decoder = JSONDecoder()
                do {
                    let address = try decoder.decode(AllData.self, from: jsonData)
                    completion?(.success(address))
                } catch {
                    completion?(.failure(error))
                }
            }
        }
    }
    task.resume()
}

//MARK: Get all categories
func getAllCategories(completion: ((NewResult<AllCategories>) -> Void)?){
    var urlComponents = URLComponents()
    urlComponents.scheme = "https"
    urlComponents.host = "www.thecocktaildb.com"
    urlComponents.path = "/api/json/v1/1/list.php"
    urlComponents.queryItems = [URLQueryItem(name: "c", value: "list")]
    guard let url = urlComponents.url else {
        print("__error: Could not create URL from components")
        return
    }
    var request = URLRequest(url: url)
    request.setValue("application/json", forHTTPHeaderField: "Content-Type")
    request.setValue("application/json", forHTTPHeaderField: "Accept")
    request.httpMethod = "GET"

    let session = URLSession.shared
    let task = session.dataTask(with: request) { (responseData, response, error) in
        DispatchQueue.main.async {
            if let error = error {
                completion?(.failure(error))
            } else if let jsonData = responseData {
                let decoder = JSONDecoder()
                do {
                    let address = try decoder.decode(AllCategories.self, from: jsonData)
                    completion?(.success(address))
                } catch {
                    completion?(.failure(error))
                }
            }
        }
    }
    task.resume()
}

