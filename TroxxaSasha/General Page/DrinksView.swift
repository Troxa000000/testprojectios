//
//  TestView.swift
//  TroxxaSasha
//
//  Created by Александр on 12.04.2020.
//  Copyright © 2020 Александр. All rights reserved.
//

import UIKit
import PinLayout

class DrinksView: UIView {
    
    let topBackView = UIView()
    let topBackViewText = UILabel()
    let headerText = UILabel()
    let drinksTableView = UITableView()
    let filterButton = UIButton()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit() {
        self.backgroundColor = .white
        addSubview(topBackView)
        topBackView.backgroundColor = .white
        topBackView.layer.shadowColor = UIColor.gray.cgColor
        topBackView.layer.shadowOpacity = 1
        topBackView.layer.shadowOffset = .zero
        topBackView.layer.shadowRadius = 5
        topBackView.addSubview(topBackViewText)
        topBackViewText.text = "Drinks"
        topBackViewText.textColor = UIColor(red: 0, green: 0, blue: 0, alpha: 1)
        topBackViewText.font = UIFont.systemFont(ofSize: 24, weight: .bold)
        topBackView.addSubview(filterButton)
        filterButton.setImage(UIImage(named: "filter"), for: .normal)
        
        addSubview(drinksTableView)

        addSubview(headerText)
        headerText.text = "Ordinary Drink"
        headerText.textColor = UIColor(red: 0.496, green: 0.496, blue: 0.496, alpha: 1)
        headerText.font = UIFont.systemFont(ofSize: 15, weight: .medium)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        topBackView.pin.top().horizontally().height(70)
        topBackViewText.pin.vertically().left(30).right()
        filterButton.pin.vertically(23).right(20).width(20)
        headerText.pin.below(of: topBackView).right().height(20).marginTop(20).left(20)
        drinksTableView.pin.below(of: headerText).horizontally().bottom()
    }
}
