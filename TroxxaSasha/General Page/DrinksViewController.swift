//
//  ViewController.swift
//  TroxxaSasha
//
//  Created by Александр on 12.04.2020.
//  Copyright © 2020 Александр. All rights reserved.
//

import UIKit
import CCBottomRefreshControl

class DrinksViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    let mainView = DrinksView()
    private let reuseIdentifierTableView = "CellTableView"
    var apiData: AllData?
    let bottomRefresh = UIRefreshControl()
    var allPages = Int()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = true
        configView()
        getApiData()
    }
    
    //MARK: Work iwith tableView
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        110
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        self.apiData?.drinks?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifierTableView, for: indexPath) as! DrinksCell
        let imageUrl = URL(string: self.apiData?.drinks?[indexPath.row].strDrinkThumb ?? "")!
        cell.imageDrink.load(url: imageUrl)
        cell.drinkName.text = self.apiData?.drinks?[indexPath.row].strDrink
        cell.selectionStyle = .none
        return cell
    }
    
    //MARK: Config View
    func configView() {
        self.view.addSubview(mainView)
        self.mainView.frame = self.view.frame
        self.mainView.drinksTableView.delegate = self
        self.mainView.drinksTableView.dataSource = self
        self.mainView.drinksTableView.separatorStyle = .none
        bottomRefresh.addTarget(self, action: #selector(refreshSimilar), for: .valueChanged)
        mainView.drinksTableView.register(DrinksCell.self, forCellReuseIdentifier: reuseIdentifierTableView)
        self.mainView.drinksTableView.bottomRefreshControl = bottomRefresh
        mainView.filterButton.addTarget(self, action: #selector(filterPressed), for: .touchUpInside)
        self.title = ""
    }
    
    //MARK:Actions
    @objc func filterPressed() {
        let vc = FilterVIewController()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    //Mark: Work with pagination
    @objc func refreshSimilar() {
        if allPages >= 1 {
            self.bottomRefresh.endRefreshing()
            return
        } else {
            getAllCocktails() { (result) in
                switch result {
                case .success(let data):
                    self.allPages += 1
                    for drink in data.drinks! {
                        self.apiData?.drinks?.append(drink)
                    }
                    self.mainView.drinksTableView.reloadData()
                    self.bottomRefresh.endRefreshing()
                case .partialSuccess( _): break
                case .failure(let error):
                    print(error)
                }
            }
        }
    }
    
    //Mark: Work withApi
    func getApiData() {
        getAllCocktails() { (result) in
            switch result {
            case .success(let data):
                self.apiData = data
                self.mainView.drinksTableView.reloadData()
            case .partialSuccess( _): break
            case .failure(let error):
                print(error)
            }
        }
    }
}

extension UIImageView {
    func load(url: URL) {
        DispatchQueue.global().async { [weak self] in
            if let data = try? Data(contentsOf: url) {
                if let image = UIImage(data: data) {
                    DispatchQueue.main.async {
                        self?.image = image
                    }
                }
            }
        }
    }
}

